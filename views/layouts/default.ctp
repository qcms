<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<?php echo $html->charset(); ?>
	<title>
		<?php __('CakePHP: the rapid development php framework:'); ?>
		<?php echo $title_for_layout;?>
	</title>
	<?php
		echo $html->meta('icon');
		echo $html->css('cake.generic');
		echo $scripts_for_layout;
	?>

	<!--[if IE]><?php echo $javascript->link('firebug/firebug'); ?><![endif]-->
</head>
<body>
	<div id="container">
		<div id="header">
			<h1><?php echo Configure::read('Site.name'); ?></h1>
		</div>
		<div id="content">
			<?php
				if ($session->check('Message.flash')):
						$session->flash();
				endif;

				if ($session->check('Message.auth')):
					$session->flash('auth');
				endif;
			?>

			<?php echo $content_for_layout;?>

		</div>
		<div id="footer">
			<?php echo $html->link($html->image('cake.power.gif', array('alt'=> __("CakePHP: the rapid development php framework", true), 'border'=>"0")), 'http://www.cakephp.org/', null, null, false); ?>
		</div>
	</div>
	<?php echo $cakeDebug?>
</body>
</html>