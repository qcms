<?php

echo $form->create('User', array('action' => 'login')),

$form->inputs(array(
	'username' => array('label' => 'Username'),
	'password'=> array('label' => 'Password'),
	'legend' => 'Login'
)),

$form->end('Login');

?>
