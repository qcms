<h2>Setup users and passwords</h2>

<p><?php printf('Use the form below to create the first user. That user will have all permissions set and will be the %s main administrator.', Configure::read('Settings.name'));?></p>


<div class="user form">
<?php echo $form->create('User', array('url' => array('controller' => 'setup', 'action' => 'create_users')));?>
	<fieldset>
 		<legend>Add User</legend>
	<?php
		echo $form->input('real_name');
		echo $form->input('email');
		echo $form->input('username');
		echo $form->input('password', array('label' => 'Password'));
		echo $form->input('password_confirm', array('type' => 'password', 'label' => 'Confirm password'));
	?>
	</fieldset>
<?php echo $form->end('Create');?>
</div>
