<h2><?php printf('Welcome to the %s setup wizard.',  Configure::read('Settings.name')); ?></h2>

<p>This wizard will guide you through the setup process.
By completing the simple steps outlined below,
the application will be ready to begin used.</p>

<h3>Steps:</h3>

<ol>
	<li>Setup a database connection</li>
	<li>Create database tables</li>
	<li>Setup users and passwords</li>
	<li>End</li>
</ol>

<p>
<?php echo $html->link('Next >>', array('action' => 'db_connection')); ?>
</p>