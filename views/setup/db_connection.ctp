<h2>Setup database connection</h2>
<?php if ($connected): ?>
	<?php extract($connection['DbConnection']); ?>
	<p><?php printf( '%s is able to connect to the database.', Configure::read('Settings.name')); ?><p>

	<dl>
		<dt>Connection</dt>
		<dd><?php printf('%s://%s:XXX@%s%s/%s', $driver, $login, $host, $port?':'.$port:'' ,$database); ?></dd>
	</dl>

	<p><?php echo $html->link('Edit', array('action' => 'db_connection', 'edit')); ?> | <?php echo $html->link('Next >>', array('action' => 'db_schema')); ?></p>
	
<?php else: ?>

	<p><?php printf( '%s is not able to connect to the database. Please check your setup.', Configure::read('Settings.name')); ?><p>

<?php endif; ?>

<?php if ($edit): ?>

	<?php
		$fields = array('legend' => 'Database Connection', 'driver', 'persistent', 'host',
			'port', 'login', 'password', 'database', 'prefix');

		echo $form->create('DbConnection', array('url' => array('controller' => 'setup', 'action' => 'db_connection'))),
		$form->inputs($fields),
		$form->end('Test');
	?>

<?php endif; ?>