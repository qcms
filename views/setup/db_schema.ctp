<h2>Create database tables</h2>

<?php if(($run)): ?>
	<?php if($success): ?>
		<p>All tables created succesfully.</p>
		<p><?php echo $html->link('Next >>', array('action' => 'create_users')); ?></p>
	<?php else: ?>
		<p>An error has been detected.</p>
		<p><?php echo $html->link('Retry', array('action' => 'db_schema', 'create')) ?></p>
	<?php endif; ?>
<?php else: ?>
	<p>Click the link below to create tables.</p>
	<p><?php echo $html->link('Create', array('action' => 'db_schema', 'create')) ?></p>
<?php endif; ?>



<?php if (isset($querys) && !empty($querys)): ?>

	<h3>Executed Queries</h3>
	<ul>
	<?php foreach($querys as $query): ?>
		<li><pre><?php echo $query ?></pre></li>
	<?php endforeach; ?>
	</ul>
<?php endif; ?>

<?php if (isset($errors) && !empty($errors)): ?>

	<h3>Errors</h3>
	<ul>
	<?php foreach($errors as $error): ?>
		<li><pre><?php echo $error ?></pre></li>
	<?php endforeach; ?>
	</ul>
<?php endif; ?>