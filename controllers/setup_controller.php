<?php
/**
* @author dardo
* @since 05/01/2008
*/
class SetupController extends AppController {
	var $name = 'Setup';
	var $uses = array('DbConnection');

	var $skipComponents = array('start', 'db_connection', 'db_schema');

	/**
	* @var SessionComponent
	*/
	var $Session;

	/**
	* @var DbConnection
	*/
	var $DbConnection;

	function start() {
	}

	function db_connection($action = '') {
		if (!empty($this->data)) {
			$this->DbConnection->save($this->data);
			$this->redirect(array('action' => 'db_connection'));
		}

		if (!$this->DbConnection->exists()) {
			$connection = array( 'DbConnection' => array(
				'driver' => 'mysql',
				'persistent' => false,
				'host' => 'localhost',
				'port' => '',
				'login' => 'root',
				'password' => '',
				'database' => strtolower(Configure::read('Settings.name')),
				'prefix' => ''
			));

			$this->DbConnection->save($connection);
			$this->redirect(array('action' => 'db_connection'));
		}

		$connection = $this->DbConnection->read();
		$this->data = $this->DbConnection->read();

		$connected = $this->_checkDbConnection();

		$edit = ($action == 'edit') || !$connected;

		$this->set(compact('connected', 'connection', 'edit'));
	}

	function db_schema($action = null) {
		$success = false;
		$run = false;
		if ($action == 'create') {
			$success = true;
			$run = true;

			uses('file', 'model' . DS . 'connection_manager', 'model' . DS . 'schema');

			$name = null;
			$path = null;
			$file = null;

			$this->Schema =& new CakeSchema(compact('name', 'path', 'file'));

			$options = array('name' => ucfirst(Configure::read('Settings.name')), 'file' => $this->Schema->file);
			$dbSchema = $this->Schema->load($options);

			$db =& ConnectionManager::getDataSource($this->Schema->connection);

			$drop = $create = array();

			foreach ($dbSchema->tables as $table => $fields) {
				$drop[$table] = $db->dropSchema($dbSchema, $table);
				$create[$table] = $db->createSchema($dbSchema, $table);
			}

			//debug($drop);
			//debug($create);

			$querys = array();

			foreach($drop as $query) {
				$querys[] = $query;
			}

			foreach($create as $query) {
				$querys[] = $query;
			}

			if (!$this->__runSql($drop, 'drop')) {
				$success = false;
			}

			if (!$this->__runSql($create, 'create')) {
				$success = false;
			}

			$this->set('querys', $querys);
			$this->set('errors', $this->__sqlErrors);
		}
		$this->set(compact('success', 'run'));
	}

	function create_users() {
		if (App::import('model', 'User')) {
			$User = new User();
		} else {
			$this->Session->setFlash('An error has occurred. Check your setup.');
			$this->redirect(array('action' => 'db_connection'));
		}

		if ($User->find('count')) {
			$this->Session->setFlash('A user already exists in the database. Cannot create another in the setup.');
			$this->redirect(array('action' => 'end'));
		}

		if(!empty($this->data)) {
			App::import('component', 'Security');

			$userdata = $this->data['User'];
			$userdata['enabled'] = '1';
			$userdata['password'] = Security::hash(Configure::read('Security.salt') . $userdata['password']);

			if ($User->save($userdata)) {
				$this->_setupACL($User->id);
				$this->redirect(array('action' => 'end'));
			}
		}
	}

	function end() {
		$setup = Configure::read('Setup');
		$setup['has_run'] = true;

		$this->_storeConfig('Setup', $setup, true);
	}

	/**
	 * Checks database connection
	 *
	 * @return Boolean
	 */
	function _checkDbConnection($datasource = 'default') {
		uses('model' . DS . 'connection_manager');
		$db =& ConnectionManager::getInstance();
		$ds =& $db->getDataSource($datasource);

		return $ds->isConnected();
	}

	function __runSql($contents, $event) {

		if (empty($contents)) {
			return true;
		}

		Configure::write('debug', 2);

		$db =& ConnectionManager::getDataSource($this->Schema->connection);
		$db->fullDebug = true;

		$errors = array();

		foreach($contents as $table => $sql) {
			if (!empty($sql)) {
				if (!$this->Schema->before(array($event => $table))) {
					return false;
				}
				if (!$db->_execute($sql)) {
					$error = $db->lastError();
					$errors[] = $error;
				}

				$this->Schema->after(array($event => $table, 'errors' => $errors));
			}
		}

		$this->__sqlErrors = $errors;

		return empty($errors);
	}

	function _setupAcl($id) {
		if (App::import('Component', 'Acl')) {
			$this->Acl =& new AclComponent();
			$this->Acl->startup($this);
		}

		$options = Configure::read('Settings.auth.options');
		$actionPath = $options['actionPath'];

		$parentId = null;

		$aco = $this->Acl->Aco;
		if ($actionPath != '/') {
			$actionPath = str_replace('/', '', $actionPath);
			$aco->create(array('alias' => $actionPath));
			$aco->save();
			$parentId = $aco->id;
		}

		Configure::load('controllers');

		$controllers = Configure::read('Controllers');

		foreach ($controllers as $controller => $actions) {
			$aco->create(array('alias' => $controller, 'parent_id' => $parentId));
			$aco->save();
			$controllerId = $aco->id;
			foreach ($actions as $action) {
				$aco->create(array('alias' => $action, 'parent_id' => $controllerId));
				$aco->save();
			}
		}

		$this->Acl->allow(array('model' => 'User', 'foreign_key' => $id),  $actionPath);
	}

	function _storeConfig($name, $data = array(), $reRead = false) {

		$content = '';
		foreach ($data as $key => $value) {
			$content .= sprintf("\$config['%s']['%s'] = %s;\n", $name, $key, var_export($value, true));
		}

		$content = "<?php\n".$content."?>";

		uses('File');
		$name = strtolower($name);
		$file = new File(CONFIGS.$name.'.php');
		if ($file->open('w')) {
			$file->append($content);
		}
		$file->close();

		if ($reRead) {
			Configure::load($name);
		}
	}

	function _initComponents() {
		if(!Configure::read('Setup.has_run')) {
			$components = array_flip($this->components);
			$blackList = array('Acl', 'Auth');
			foreach ($blackList as $key) {
				if (isset($components[$key])) {
					unset($components[$key]);
				}
			}

			$this->components = array_flip($components);
		}
		return parent::_initComponents();
	}

}
?>