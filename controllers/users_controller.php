<?php
/**
 * @author dardo
 * @since 25/03/2008
 */
class UsersController extends AppController {
	var $name = 'Users';

	var $allowedActions = array('admin_login');

	function login() {
	}

	function logout() {
		$this->Auth->logout();
		$this->redirect(array('controller' => 'Users', 'action' => 'login'));
	}

	function admin_login(){
		$this->setAction('login');
	}

	function admin_logout(){
		$this->setAction('logout');
	}
}
?>