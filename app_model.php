<?php
/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 */
class AppModel extends Model{

	/**
	 * Get the ACL parent node
	 *
	 * Defined as empty, so any model can be used in ACL.
	 * Every model should override it if necessary.
	 */
	function parentNode() {
	}
}
?>