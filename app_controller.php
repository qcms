<?php
class AppController extends Controller {

	var $components = array('RequestHandler', 'Acl', 'Auth');
	var $helpers = array('Html', 'Form');

	var $allowedActions = array();

	function beforeFilter() {
		Configure::write('Config.language', 'en');

		$this->_setDebug();
		$this->_setupAuth();
	}

	function _setDebug(){
		if (Configure::read('debug') > 1) {
			if ($this->RequestHandler->isAjax()) {
				Configure::write('ajaxdebug', true);
			}
		}
	}

	function _setupAuth() {
		if (isset($this->Auth)) {
			if (Configure::read('Settings.auth.enabled')) {

				$availableOptions = array_keys(get_object_vars($this->Auth));
				$options = Configure::read('Settings.auth.options');

				foreach ($options as $option => $value) {
					if (in_array($option, $availableOptions)) {
						$this->Auth->$option = $value;
					}
				}

				if (!empty($this->allowedActions)) {
					$this->Auth->allow($this->allowedActions);
				}

			} else {
				$this->Auth->allow();
			}
		}
	}
}
?>