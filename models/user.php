<?php
class User extends AppModel {
	var $name = 'User';

	var $validate = array(
		'real_name' => array(
			'empty' => array(
				'rule' => VALID_NOT_EMPTY,
				'required' => true,
				'allowEmpty' => false,
				'message' => 'Real name can\'t be left empty.',
			)
		),
		'username' => array(
			'alphanumeric' => array(
				'rule' => 'alphaNumeric',
				'required' => true,
				'message' => 'Username must be alphanumeric characters only.'
			),
			'unique' => array(
				'rule' => array('unique', 'username'),
				'required' => true,
				'message' => 'That username arlready exists.'
			)
		),
		'password' => array(
			'empty' => array(
				'rule' => 'checkPasswordNotEmpty',
				'required' => false,
				'message' => 'Password can\'t be left empty.',
				'on' => 'create'
			)
		),
		'password_confirm' => array(
			'checkpassword' => array(
				'rule' => 'confirmPassword',
				'required' => false,
				'message' => 'Passwords don\'t match.'
			)
		),
		'email' => array(
			'email' => array(
				'rule' => 'email',
				'required' => true,
				'message' => 'Must be a valid e-mail address.',
				'allowEmpty' => false
			)
		)
	);

	var $hasOne = array();
	var $hasMany = array();
	var $belongsTo = array();
	var $hasAndBelongsToMany = array();

	var $actsAs = array('Acl' => 'requester');

	function confirmPassword() {
		$passwordConfirm = Security::hash(Configure::read('Security.salt') . $this->data['User']['password_confirm']);
		$password = $this->data['User']['password'];

		return $password == $passwordConfirm ;
	}

	function checkPasswordNotEmpty($data) {
		App::import('component', 'Security');
		$emptyPasswordHashed = Security::hash(Configure::read('Security.salt'));
		return $data['password'] !=  $emptyPasswordHashed;
	}

	function unique($data, $name){
		$this->recursive = -1;
		if (!empty($data[$name])) {
			$found = $this->find(array("{$this->name}.$name" => '= '.$data[$name]));
			$same = isset($this->id) && $found[$this->name][$this->primaryKey] == $this->id;
	
			return !$found || $found && $same;
		}
		return true;
	}
	
	function beforeSave() {
		return true;
	}
}
?>