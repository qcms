<?php
/**
* @author dardo
* @since 05/01/2008
*/
class DbConnection extends AppModel {
	var $name = 'DbConnection';

	var $useTable = false;

	var $validate = array();

	var $fields = array(
		'driver' => 'string',
		'persistent' => 'boolean',
		'host' => 'string',
		'port' => 'integer',
		'login' => 'string',
		'password' => 'string',
		'database' => 'string',
		'prefix' => 'string',
	);

	function save($data, $validate = true) {

		extract($data['DbConnection']);

		if (empty($driver)) {
			$driver = 'mysql';
		}

		if ($persistent) {
			$persistent = 'true';
		} else {
			$persistent = 'false';
		}

		$config = "<?php
class DATABASE_CONFIG {
	var \$default = array(
		'driver' => '$driver',
		'persistent' => $persistent,
		'host' => '$host',
		'port' => '$port',
		'login' => '$login',
		'password' => '$password',
		'database' => '$database',
		'schema' => '',
		'prefix' => '$prefix',
		'encoding' => 'UTF8'
	);
};
?>";
		uses('File');
		$file = new File(CONFIGS.'database.php');
		$file->write($config, 'w');
		$file->close();
	}

	function read($datasource = 'default') {
		uses('model' . DS . 'connection_manager');
		$db = ConnectionManager::getInstance();
		$ds = $db->getDataSource($datasource);

		return array('DbConnection' => $ds->config);
	}

	function exists($reset = false) {
		uses('File');
		$file = new File(CONFIGS.'database.php');
		return $file->exists();
	}

}
?>