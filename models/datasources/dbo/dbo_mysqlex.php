<?php

require_once (LIBS . 'model' . DS . 'datasources' . DS . 'dbo' . DS . 'dbo_mysql.php');

/**
 * MySQL Extended datasource
 *
 * Overrides MySQL Datasource log functions to log in the Firebug console
 * Taken from http://bin.cakephp.org/view/27213675 and refactored a litle
 *
 * @author Sharkoon <sharkoon@gmail.com>
 *
 */
class DboMysqlEx extends DboMysql {

	var $description = "MySQL DBO Driver - Extended";
	
	function logQuery($sql) {
		$ret = parent::logQuery($sql);
		
		$this->log($this->_queriesLog[count($this->_queriesLog)-1], 'sql');
		return $ret;
	}

	function showLog($sorted = false) {

		if (!Configure::read('ajaxdebug')) {
			return parent::showLog($sorted);
		}

		if(Configure::read() < 2) {
			return;
		}

		$consoleDescribes = $consoleSql = '';
		$errors = 0;

		if ($sorted) {
			$log = sortByKey($this->_queriesLog, 'took', 'desc', SORT_NUMERIC);
		} else {
			$log = $this->_queriesLog;
		}


		if (php_sapi_name() == 'cli') {
			foreach ($log as $k => $i) {
				print (($k + 1) . ". {$i['query']} {$i['error']}\n");
			}
			return;
		}

		foreach ($log as $k => $i) {
			if (strpos($i['query'], 'DESCRIBE') !== false) {
				$consoleDescribes .= "console.log('".addslashes(h($i['query']))."');\n";
			} else {
				$consoleSql .= "console.info('" . ($k + 1) . ". Affected: {$i['affected']}   Num. rows: {$i['numRows']} Took: {$i['took']} ms');\n"; // Query information
				if($i['error']) {
					$errors++;
					$consoleSql .= "console.error('error: ".addslashes($i['error'])."');\n"; // Query error
				}
				$consoleSql .= "console.log('" . addslashes($i['query']) . " ');\n"; // Query
				$consoleSql .= "console.log(' ');\n"; // New line
			}
		}
		$this->__printJsLog($consoleDescribes, $consoleSql, $errors);
	}

	function __printJsLog($consoleDescribes, $consoleSql, $errors) {

		$text = ($this->_queriesCnt > 1) ? 'queries' : 'query';

		print ("<script type=\"text/javascript\">" .
				"if(window.console){" .
					"".($errors ? "console.error('".$errors." SQL errors');\n" : "")."" .

					"console.group('SQL Describes');\n" .
					"".$consoleDescribes."" .
					"console.groupEnd();\n" .

					"console.group('SQL {$text}');\n" .
					"".$consoleSql . "" .
					"console.groupEnd();\n" .

					"console.info('{$this->_queriesCnt} {$text} took {$this->_queriesTime} ms');\n" .
					"".($errors ? "console.error('".$errors." SQL errors');\n" : "")."" .
				"}" .
			"</script>");
	}


}
?>