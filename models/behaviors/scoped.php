<?php
/**
 * Automatically adds dynamic find conditions to model
 * 
 * @author Dardo Sordi <dardosordi@gmail.com>
 * @author Leandro Lopez <lopezlean@gmail.com>
 *
 */
class ScopedBehavior extends ModelBehavior {

	/**
	 * Stores model specific settings
	 *
	 * @var Array
	 */
	var $settings = array();

	/**
	 * Stores global pseudoconstants
	 *
	 * @var Array
	 */
	var $constants = array();

	function __construct() {
		parent::__construct();
		$this->globals = array(
			'CURRENT_DATE' => date('Y-m-d'),
			'NOW' => date('Y-m-d H:i:s')
		);
	}

	/**
	 * Initializes model settings
	 *
	 * @param Model $model
	 * @param Array $settings
	 */
	function setup(&$model, $settings) {
		$default = array('conditions' => array($model->alias.'.enabled' => 1) , 'enabled' => true, 'constants' => array());
		$this->settings[$model->alias] = array_merge($default, $settings);
	}

	/**
	 * Enables ScopedBehavior on this model
	 *
	 * @param Model $model
	 */
	function enableScope(&$model) {
		$this->settings[$model->alias]['enabled'] = true;
	}

	/**
	 * Disables ScopedBehavior on this model
	 *
	 * @param Model $model
	 */
	function disableScope(&$model) {
		$this->settings[$model->alias]['enabled'] = false;
	}

	/**
	 * Adds a scope condition for this model
	 *
	 * @param Model $model
	 * @param String $name
	 * @param String $value
	 */
	function addToScope(&$model, $name, $value = null) {
		if (is_array($name)) {
			$this->settings[$model->alias]['conditions'] = array_merge($this->settings[$model->alias]['constants'], $name);
		} else {
			$this->settings[$model->alias]['conditions'][$name] = $value;
		}
	}

	/**
	 * Unsets a scope condition for this model
	 *
	 * @param Model $model
	 * @param Mixed $name
	 */
	function removeFromScope(&$model, $name) {
		if (is_array($name)) {
			foreach ($name as $key) {
				unset ($this->settings[$model->alias]['conditions'][$key]);
			}
		} else {
			unset ($this->settings[$model->alias]['conditions'][$name]);
		}
	}

	/**
	 * Sets the current scope for this model
	 *
	 * @param Model $model
	 * @param Array $scope
	 */
	function setScope(&$model, $scope) {
		$this->settings[$model->alias]['conditions'] = $scope;
	}

	/**
	 * Gets the current scope for this model
	 *
	 * @param Model $model
	 * @return Array
	 */
	function getScope(&$model) {
		return $this->settings[$model->alias]['conditions'];
	}

	/**
	 * Defines a pseudoconstant for this model
	 * 
	 * It can handle an associative array in the form 'name' => 'value'
	 *
	 * @param Model $model
	 * @param String $name
	 * @param Mixed $value
	 */
	function setConstant(&$model, $name , $value = null) {
		if (is_array($name)) {
			$this->settings[$model->alias]['constants'] = array_merge($this->settings[$model->alias]['constants'], $name);
		} else {
			$this->settings[$model->alias]['constants'][$name] = $value;
		}
	}

	/**
	 * Unsets a pseudconstant for this model
	 *
	 * @param Model $model
	 * @param Mixed $name
	 */
	function removeConstant(&$model, $name) {
		if (is_array($name)) {
			foreach ($name as $key) {
				unset ($this->settings[$model->alias]['constants'][$key]);
			}
		} else {
			unset ($this->settings[$model->alias]['constants'][$name]);
		}
	}

	/**
	 * Returns the value of a pseudconstant for this model
	 *
	 * @param Model $model
	 * @param String $name
	 * @return Mixed
	 */
	function getConstant(&$model, $name) {
		if (is_array($name)) {
			$ret = array();
			foreach ($name as $key) {
				$ret[$key] = $this->settings[$model->alias]['constants'][$key];
			}
			return $ret;
		} else {
			return $this->settings[$model->alias]['constants'][$name];
		}
	}

	/**
	 * Add conditions before model find
	 *
	 * @param Model $model
	 * @param Array $query
	 * @return Array
	 */
	function beforeFind(&$model, $query) {
		extract($this->settings[$model->alias]);

		if (!$enabled) {
			return $query;
		}

		if (!isset($query['conditions'])) {
			$query['conditions'] = array();
		}

		if (is_string($query['conditions'])) {
			$query['conditions'] = array($query['conditions']);
		}

		$conditions = $this->__replacePseudoConstants($model, $conditions);

		$query['conditions'] = array_merge($query['conditions'], $conditions);
		return $query;
	}

	/**
	 * Replaces each placeholder in the conditions array by its definded value
	 *
	 * @param Model $model
	 * @param Array $conditions
	 * @return Array
	 */
	function __replacePseudoConstants(&$model, $conditions) {
		$constants = array_merge($this->globals,$this->settings[$model->alias]['constants']);

		foreach ($conditions as $idx => $value) {
			$conditions[$idx] = $this->__expandVariables($value , $constants);
		}
		return $conditions;
	}

	/**
	 * Replace each placeholder in the string by its defined value, leaves undefinded untouched
	 *
	 * @param String $value
	 * @param Array $constants
	 * @return String
	 */
	function __expandVariables($value , $constants) {
		$ret = null;
		preg_match_all('/{\w+}/', $value, $ret);
		$orig = $dest = array();

		if(!empty ($ret[0])) {
			foreach($ret[0] as$var) {
				$key = str_replace(array('{', '}'), '', $var);
				if (isset ($constants[$key])) {
					$orig[] = $var;
					$dest[] = $constants[$key];
				}
			}
			if (!empty($dest)) {
				return str_replace($orig, $dest, $value);
			}
		}
		return $value;
	}
}


?>