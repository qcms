<?php
class Page extends AppModel {
	var $name = 'Page';

	var $actsAs = array(
		'Tree',
		'Sluggable' => array('translation' => 'utf-8', 'overwrite' => true),
		'Scoped' => array('conditions' => array('Page.published' => '<= {NOW}'))
	);

	var $validate = array(
		'title' => array(
			'rule' => array('minlength', 1),
			'message' => 'The title should not be empty.'
		),
		'content' => array(
			'rule' => array('minlength', 1),
			'message' => 'The content should not be empty.'
		)
	);

	var $hasOne = array();
	var $hasMany = array();
	var $belongsTo = array();
	var $hasAndBelongsToMany = array();

	function treeList($space = ' '){
		return $this->generateTreeList(null, '{n}.Page.id', '{n}.Page.title', $space);
	}

	function beforeSave() {
		if ($this->data['Page']['published'] == 1) {
			$this->data['Page']['published'] = date('Y-m-d H:i:s');
		} elseif ($this->data['Page']['published'] == 0) {
			$this->data['Page']['published'] = null;
		}
		return true;
	}

}
?>