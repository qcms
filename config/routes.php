<?php
	if (Configure::read('Setup.has_run')) {
		Router::connect('/', array('controller' => 'pages', 'action' => 'display', 'home'));
		Router::connect('/login', array('controller' => 'users', 'action' => 'login'));
		Router::connect('/logout', array('controller' => 'users', 'action' => 'logout'));
	} else {
		Router::connect('/', array('controller' => 'setup', 'action' => 'start'));
		Router::connect('/admin/*', array('controller' => 'setup', 'action' => 'start'));
		Router::connect('/:action', array('controller' => 'setup'));
	}

	//Router::connect('/pages/*', array('controller' => 'pages', 'action' => 'display'));

	Router::connect('/tests', array('controller' => 'tests', 'action' => 'index'));
?>