<?php
$config['Settings']['name'] = 'QCMS';
$config['Settings']['auth'] = array (
  'enabled' => true,
  'options' => 
  array (
    'authorize' => 'actions',
    'actionPath' => 'Controllers/',
    'loginAction' => 
    array (
      'controller' => 'users',
      'action' => 'login',
    ),
    'loginRedirect' => '/',
    'logoutRedirect' => 
    array (
      'controller' => 'users',
      'action' => 'login',
    ),
    'autoRedirect' => true,
    'ajaxLogin' => 'ajax_login',
    'loginError' => 'Authentication failed. Please, try again.',
    'authError' => 'You are not authorized to reach that location.',
    'userModel' => 'User',
    'fields' => 
    array (
      'username' => 'username',
      'password' => 'password',
    ),
    'userScope' => 
    array (
      'User.enabled' => 1,
    ),
  ),
);
?>