<?php

Configure::load('setup');
Configure::load('controllers');
Configure::load('settings');
Configure::load('site');

function url(){

	$argc = func_num_args();
	$argv = func_get_args();

	if ($argc == 1 && is_array($argv[0])) {
		$argv = $argv[0];
		$argc = count($argv);
	}

	switch ($argc) {
		case 1:
			if (strpos($argv[0], '/')!== false) {
				return url(explode('/', ltrim($argv[0], '/')));
			}
			return array('action' => $argv[0]);
		case 2:
			return array('controller' => $argv[0], 'action' => $argv[1]);
		case 3:
			return array('controller' => $argv[0], 'action' => $argv[1], $argv[2]);
		default:
			$controller = array_shift($argv);
			$action = array_shift($argv);
			return array_merge(array('controller' => $controller, 'action' => $action), $argv);
	}
}

?>