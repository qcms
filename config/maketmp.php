<?php

$tmpBase = ROOT.DS.APP_DIR.DS.'tmp'.DS;

if (!(is_dir($tmpBase) && is_writable($tmpBase))) {

	$tmpDirs = array(
		'',
		'logs',
		'cache',
		'cache/views',
		'cache/persistent',
		'cache/models',
		'tests',
		'sessions',
	);

	$fileMode = 0777;
	foreach ($tmpDirs as $dir) {
	
		$path = $tmpBase.$dir;
	
		if (!is_dir($path)) {
			@mkdir($path, $fileMode);
		}
	
		if (!is_writable($path)) {
			@chmod($path, $fileMode);
		}
	}
}
?>